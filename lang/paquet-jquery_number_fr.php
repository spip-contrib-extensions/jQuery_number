<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// j
	'jquery_number_description' => 'Encapsulation de la librairie jQuery Number',
	'jquery_number_slogan' => 'Formatez vos nombres !'
);
